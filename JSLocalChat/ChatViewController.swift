//
//  ChatViewController.swift
//  JSLocalChat
//
//  Created by Jerzyk on 16/01/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation
import UIKit

protocol ChatViewControllerDelegate {
    func ChatViewControllerClosedConnection(controller: ChatViewController)
}

class ChatViewController: UIViewController, UITableViewDataSource, ConnectionManagerDelegate {
    
    static let storyboardNibName = "Main"
    static let storyboardVCIdentifier = "ChatViewControllerStoryboardID"
    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextField: UITextField!
    
    var delegate: ChatViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatTableView.dataSource = self
        
        //dirty hack to intercept back button action
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Disconnect", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed(sender:)))

        self.navigationItem.leftBarButtonItem = newBackButton;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ConnectionManager.sharedInstance.delegate = self
    }
    
    func backButtonPressed(sender: UIBarButtonItem) {
        ConnectionManager.sharedInstance.disconnect()
    }
    
    //---------------------------------
    //MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ConnectionManager.sharedInstance.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = chatTableView.dequeueReusableCell(withIdentifier: "chatCellID", for: indexPath)
        let message = ConnectionManager.sharedInstance.messages[indexPath.row]
        
        cell.textLabel?.text = message
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    //---------------------------------
    @IBAction func sendButtonPressed(_ sender: Any) {
        if let message = self.messageTextField.text {
            self.messageTextField.text = ""
            ConnectionManager.sharedInstance.send(message: message)
        }
    }
    
    //-------------------------------------------
    //MARK: - Delegate methods
    //MARK: ConnectionManager
    func MessagesUpdated() {
        self.chatTableView.reloadData()
        
        let lastMsg = IndexPath.init(row: ConnectionManager.sharedInstance.messages.count-1, section: 0)
        self.chatTableView.scrollToRow(at: lastMsg, at: UITableViewScrollPosition.middle, animated: true)
    }
    
    func ConnectionClosed() {
        delegate?.ChatViewControllerClosedConnection(controller: self)
    }
}
