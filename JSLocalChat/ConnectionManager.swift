//
//  ChatController.swift
//  JSLocalChat
//
//  Created by Jerzyk on 13/01/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation

@objc protocol ConnectionManagerDelegate {
    @objc optional func ConnectionInitialisedWith(result: Bool, message: String?)
    @objc optional func ConnectionClosed()
    @objc optional func MessagesUpdated()
}

class ConnectionManager: NSObject, StreamDelegate {
    
    static let sharedInstance = ConnectionManager()
    private override init() {
        super.init()
        let userDefaults = UserDefaults.standard
        if let address = userDefaults.value(forKey: "serverAddress") {
            self.serverAddress = address as! CFString
        }
    }
    
    var delegate: ConnectionManagerDelegate?
    
    private var inputStream: InputStream!
    private var outputStream: OutputStream!
    var messages = [""]
    
    var serverAddress: CFString = "localhost" as CFString
    var portNumber: UInt32 = 80
    
    private var _isConnected = false
    var isConnected: Bool {
        get {
            return self._isConnected
        }
    }
    
    func initNetworkCommunication() {
        var readStream:  Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocketToHost(nil, self.serverAddress, self.portNumber, &readStream, &writeStream)
        
        self.inputStream = readStream!.takeRetainedValue()
        self.outputStream = writeStream!.takeRetainedValue()
        
        self.inputStream.delegate = self
        self.outputStream.delegate = self
        
        self.inputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        self.outputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        if self.inputStream != nil && self.outputStream != nil { //TODO: not working
            self._isConnected = true
        } else {
            self._isConnected = false
        }
        delegate?.ConnectionInitialisedWith?(result: self.isConnected, message: "TODO:message")
    }
    
    func disconnect() {
        self.inputStream.delegate = nil
        self.outputStream.delegate = nil
        
        self.inputStream.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        self.outputStream.remove(from: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        
        self.inputStream.close()
        self.outputStream.close()
        self._isConnected = false
        
        delegate?.ConnectionClosed?()
    }
    
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch (eventCode) {
        case Stream.Event.openCompleted:
            print("Stream openCompleted")
            break
            
        case Stream.Event.hasBytesAvailable:
            print("Stream hasBytesAvailable")
            if (aStream == inputStream) {
                
                var buffer = [UInt8](repeating: 0, count: 100)
                while inputStream.hasBytesAvailable {
                    _ = inputStream.read(&buffer, maxLength: buffer.count)
                    if let str = String(bytes: buffer, encoding: String.Encoding.utf8) {
                        print("Received:\(str)")
                        self.messages.append(str)
                        delegate?.MessagesUpdated?()
                    }
                }
            }
            break
            
        case Stream.Event.hasSpaceAvailable:
            print("Stream hasSpaceAvailable")
            break
            
        case Stream.Event.errorOccurred:
            print("Stream errorOccurred")
            self.disconnect()
            break
            
        case Stream.Event.endEncountered:
            print("Stream endEncountered")
            break
            
        default:
            print("Unknown event")
            self.disconnect()
        }
    }
    
    func sendJoinChatRequestWith(name:String) {
        let response = "iam:\(name)"
        DispatchQueue.main.async {
            self.writeToOutputStream(text: response)
        }
    }
    
    func send(message: String) {
        let response = "msg:\(message)"
        writeToOutputStream(text: response)
    }
    
    private func writeToOutputStream(text:String) {
        if let data = text.data(using: String.Encoding.ascii) {
            _ = data.withUnsafeBytes {
                self.outputStream.write($0, maxLength: data.count)
            }
        }
    }
}
