//
//  SettingsViewController.swift
//  JSLocalChat
//
//  Created by Jerzyk on 16/01/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController {
    
    static let storyboardNibName = "Main"
    static let storyboardVCIdentifier = "SettingsViewControllerStoryboardID"
    
    @IBOutlet weak var changeAddressButton: UIButton?
    @IBOutlet weak var serverAddressTextField: UITextField?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverAddressTextField?.text = ConnectionManager.sharedInstance.serverAddress as String
    }
    
    @IBAction func changeAddressButtonPressed(_ sender: Any) {
        if let address = serverAddressTextField?.text {
            ConnectionManager.sharedInstance.serverAddress = address as CFString
            
            let userDefaults = UserDefaults.standard
            userDefaults.setValue(address, forKey: "serverAddress")
            userDefaults.synchronize()
        }
    }
}
