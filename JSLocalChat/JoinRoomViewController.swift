//
//  ViewController.swift
//  JSLocalChat
//
//  Created by Jerzyk on 11/01/2017.
//  Copyright © 2017 JS. All rights reserved.
//

import UIKit


class JoinRoomViewController: UIViewController, StreamDelegate, ConnectionManagerDelegate, ChatViewControllerDelegate {
    
    @IBOutlet weak var enterNameLabel: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var initNetworkButton: UIButton!
    @IBOutlet weak var connectionActivityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hide keyboard after tapping outside keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(JoinRoomViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ConnectionManager.sharedInstance.delegate = self
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        let sb = UIStoryboard(name: SettingsViewController.storyboardNibName, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: SettingsViewController.storyboardVCIdentifier)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func joinButtonPressed(_ sender: Any) {
        ConnectionManager.sharedInstance.initNetworkCommunication()
        if let _ = self.nameTextField.text {
            self.joinButton.isEnabled = false
            self.connectionActivityIndicator.startAnimating()
        }
    }
    
    //-------------------------------------------
    //MARK: - Delegate methods
    //MARK: ConnectionManager
    func ConnectionInitialisedWith(result: Bool, message: String?) {
        self.joinButton.isEnabled = result
        DLog("\(message)")
        if let name = self.nameTextField.text {
            ConnectionManager.sharedInstance.sendJoinChatRequestWith(name: name)
        }
    }
    
    func MessagesUpdated() {
        print("JoinedChatRoom")
        self.connectionActivityIndicator.stopAnimating()
        let sb = UIStoryboard(name: ChatViewController.storyboardNibName, bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: ChatViewController.storyboardVCIdentifier) as? ChatViewController {
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func ConnectionClosed() {
        print("Connection closed")
        self.joinButton.isEnabled = true
        self.connectionActivityIndicator.stopAnimating()
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: ChatViewController
    func ChatViewControllerClosedConnection(controller: ChatViewController) {
        controller.delegate = nil
        self.joinButton.isEnabled = true
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}

